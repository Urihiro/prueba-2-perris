from django.db import models

# Create your models here.

class Persona(models.Model):
    rut = models.CharField(max_length=10)
    nombre = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=150)
    correo = models.EmailField(max_length=100)
    contacto = models.IntegerField()
    fecha_nacimiento = models.DateTimeField('%Y/%m/%d')
    region = models.CharField(max_length=150)
    comuna = models.CharField(max_length=150)
    tipocasa = models.CharField(max_length=90)
    contraseña = models.CharField(max_length=90)
