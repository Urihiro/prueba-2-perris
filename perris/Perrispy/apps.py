from django.apps import AppConfig


class PerrispyConfig(AppConfig):
    name = 'Perrispy'
