from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,logout, login as auth_login
from django.contrib.auth.decorators import login_required
from .models import Persona
# Create your views here.



def index(request):
    return render(request,'index.html',{'nombre':"Nea"})

def registro(request):
    
    return render(request,'Form.html',{})

def crear(request):
    rut=request.POST.get('rut',00000000)
    nombre=request.POST.get('nombre','')
    apellidos=request.POST.get('apellidos','')
    correo=request.POST.get('correo','')
    contacto=request.POST.get('contacto',123456789)
    fecha_nacimiento=request.POST.get('fecnac','01/01/18')
    region=request.POST.get('region','')
    comuna=request.POST.get('comuna','')
    tipocasa=request.POST.get('tipovivienda','')
    contraseña=request.POST.get('contraseña','')
    persona = Persona(nombre=nombre,apellidos=apellidos,correo=correo,contacto=contacto,fecha_nacimiento=fecha_nacimiento,region=region,comuna=comuna,tipocasa=tipocasa,contraseña=contraseña)
    persona.save()
    return redirect('index')

@login_required(login_url='login')
def cerrar_session(request):
    del request.session['usuario']
    logout(request)
    return redirect('index')

def login(request):
    return render(request,'Form.html',{})

def login_iniciar(request):
    usuario = request.POST.get('usuario','')
    contraseña = request.POST.get('contraseña','')
    user = authenticate(request,username=usuario, password=contraseña)
    return redirect("index")
    # if user is not None:
    #     auth_login(request, user)
    #     request.session['usuario'] = user.first_name+" "+user.last_name
    #     return redirect("index")
    # else:
    #     return redirect("login")